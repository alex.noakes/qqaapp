import { ADD_QUESTION, REMOVE_QUESTION, CLEAR_QUESTIONS } from '../constants/ActionTypes'

const initialState = {
  questions: [
    {
      question: 'Can you ask a question?',
      answer: 'Sure, just hit submit.'
    }
  ]  
}

const questions = (state = initialState, action) => {
  switch (action.type) {
    case ADD_QUESTION:
    for (let i = 0; i <  state.questions.length; i++) {
      let existingQuestion = state.questions[i];
      if (existingQuestion === action.question) {
        // The question already exists in the state list so no need to add again.
        return state; 
      }
    }
    return { 
      ...state,
      questions: state.questions.concat(action.question),
     }
    case REMOVE_QUESTION:
      return { 
        ...state,
        questions: state.questions.filter(item => item.question !== action.question.question),
      }
    case CLEAR_QUESTIONS:
      return { 
        ...state,
        questions: Object.assign([]),
      }

  default:
      return state
  }
}

export default questions