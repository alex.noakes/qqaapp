import { createStore, combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import questions from './reducers/questions';

const reducer = combineReducers({
  form: reduxFormReducer, // mounted under "form",
  questions: questions
});
const store = (window.devToolsExtension
  ? window.devToolsExtension()(createStore)
  : createStore)(reducer);

export default store;
