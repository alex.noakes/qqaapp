import React, { Component } from 'react';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import QuestionForm from './collections/Questions/QuestionForm';
import AnswerList from './collections/Questions/AnswerList';
import { reset } from "redux-form";
import store from './store';
import { addQuestion } from './actions'
import './App.css';

/**
 * Handle question form submissions. 
 * 
 * @param {object} values
 *   Form values
 */
async function submitQuestion(values) {
  const question = {
    question: values.question,
    answer: values.answer,
    display: false 
  };
  store.dispatch(addQuestion(question));
  store.dispatch(reset('QuestionForm'));
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider>
          <div className="App">
            <header className="App-header">
              <h1 className="App-title">The awesome Q/A tool</h1>
            </header>
          
            <div className="content">
              <div className="answers">
                <AnswerList/>
              </div>
              <div className="questionForm">
                <QuestionForm onSubmit={submitQuestion}/>
              </div>
            </div>
          </div>
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default App;
