import React, {Component} from 'react';
import {RaisedButton} from 'material-ui';
import { Grid, Row, Col } from 'react-material-responsive-grid';
import {connect} from 'react-redux';

class QuestionItem extends Component {

  constructor(...args) {
    super(...args);
    this.state = { 
      show: false 
    };
  }

  handleClick() {
    this.setState({
      show: !this.state.show
    });
  }
 
  render() {
     const {question, onRemove} = this.props;  

    return (
      <div>
        <div className="question-item" onClick={ () => this.handleClick() }>
          <div className="question-answer-pair">
            <div className="question tooltip">
              {question.question}
            </div>
            <span className="tooltiptext">Click me to reveal the Answer</span>
            {this.state.show &&
              <div className="answer">
              {question.answer}
              </div> 
            }
          </div>
          <div>
            <RaisedButton 
              label="Remove"
              onClick={onRemove}
              className="tooltip"
            />
            <span className="tooltiptext">Remove just an individual Question answer pair</span>
          </div>
        </div>
      </div>
    )
  };
}


class AnswerList extends Component {

  constructor(...args) {
    super(...args);
    this.state = {
      sorting: false,
      clicked: true
    };
  }

  onSort = () => {
    this.setState({
      sorting: !this.state.sorting,
    });
  }

  onClear = () => {
    this.props.clearQuestions();
  }

  render() {
    const {questions, removeQuestion} = this.props;
    const {sorting} = this.state;

    if (sorting) {
      questions.sort(function(a ,b) {return a.question.localeCompare(b.question);});
    }
     
    return (
      <Grid>
        <Row>
          <div className="answersList">
            <h2 className="tooltip">Created questions</h2>
            <span className="tooltiptext">Here you can find the created questions and their answers.</span>
            
            <Col >
              <div id='appContent'>
              {questions.length ? (
                <div id="intro">
                  Here you can find {questions.length} {questions.length !== 1 ? 'questions': 'question'} <br />
                  Feel free to create your own questions!
                </div>             
                 ) : (
                   <span>No Questions Yet ;( </span>
              )}

                
              </div>
            </Col>
            <Col>
                {questions.map(function(question, i){
                  return <QuestionItem question={question} key={i} onRemove={()=>{ removeQuestion(question) }}/>;
                })}

              <div id="questionTools">
                <RaisedButton
                  label="Sort"
                  onClick={this.onSort}
                  primary={!sorting}
                  className="tooltip"
                  />
                  <span className="tooltiptext">Toggle Sort State</span>
                <RaisedButton
                  label="Clear questions"
                  onClick={this.onClear}
                  secondary={true}
                  className="tooltip"
                />
                <span className="tooltiptext">Wipe out ALL the questions in one click!</span>
                
              </div>  
          </Col>
          </div>
        </Row>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    questions : state.questions.questions
  }
}

const mapDispatchToProps = dispatch => {
  return {
    removeQuestion : (question) => dispatch({
      type : 'REMOVE_QUESTION',
      question: question
    }),
    clearQuestions: () => dispatch({
      type : 'CLEAR_QUESTIONS',
    })
  }
}


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AnswerList)