import * as types from '../constants/ActionTypes'

export const addQuestion = question => ({ type: types.ADD_QUESTION, question })
export const removeQuestion = question => ({ type: types.REMOVE_QUESTION, question })
export const clearQuestions = question => ({ type: types.CLEAR_QUESTIONS })